package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;


import java.net.URL;
import java.nio.file.Paths;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.Function;

class Mobil {

    public String kode;
    public String nama;
    public int hargaMobil;
    public int hargaCCDC;
    public int hargaCash;
    public int hargaAC;
    public int hargaCentral;
    public int hargaKacaFilm;

    public Mobil(String kode, 
                 String nama, 
                 int hargaMobil,
                 int hargaCCDC,
                 int hargaCash,
                 int hargaAC,
                 int hargaCentral,
                 int hargaKacaFilm) {
        this.kode = kode;
        this.nama = nama;
        this.hargaMobil = hargaMobil;
        this.hargaCCDC = hargaCCDC;
        this.hargaCash = hargaCash;
        this.hargaAC = hargaAC;
        this.hargaCentral = hargaCentral;
        this.hargaKacaFilm = hargaKacaFilm;
    }

    @Override
    public String toString() {
        return this.kode;
    }
    
}

public class Controller implements Initializable{

    @FXML
    public CheckBox acCheckBox;
    @FXML
    public CheckBox centralCheckBox;
    @FXML
    public CheckBox kacaCheckBox;
    @FXML
    public ComboBox<Mobil> mobilCombobox;
    @FXML
    public TextField namaTextField;
    @FXML
    public TextField hargaTextField;
    @FXML
    public RadioButton ccdcRB;
    @FXML
    public RadioButton cashRB;
    @FXML
    public TextField hargaCCDCCashTextField;

    final ToggleGroup group = new ToggleGroup();
    @FXML
    public TextField acTextField;
    @FXML
    public TextField centralTextField;
    @FXML
    public TextField kacaTextField;
    @FXML
    public TextField totalTextField;
    private List<Mobil> mobilList = new ArrayList<>();

    private void calcTotal() {
        Function<TextInputControl, Integer> toInt =
                x -> {
                    String txt = x.getText();
                    return txt.matches("\\d+") ? Integer.parseInt(txt) : 0;
                };

        int totalHarga =
                toInt.apply(hargaTextField)
                + toInt.apply(hargaCCDCCashTextField)
                + toInt.apply(acTextField)
                + toInt.apply(centralTextField)
                + toInt.apply(kacaTextField);
        totalTextField.setText(String.valueOf(totalHarga));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String sql = "SELECT * FROM Mobil";

        String db = Paths.get(".").toAbsolutePath().normalize().toString() + "/tugas_java_2.db";
        System.out.println(db);
        try (Connection conn = DriverManager.getConnection(
                "jdbc:mysql://localhost/tugas_pbo?user=root");
             Statement stmt  = conn.createStatement();
             ResultSet rs    = stmt.executeQuery(sql)) {
            while (rs.next())
                mobilList.add(new Mobil(
                    rs.getString("kode"),
                    rs.getString("nama"),
                    rs.getInt("hargaMobil"),
                    rs.getInt("hargaCCDC"),
                    rs.getInt("hargaCash"),
                    rs.getInt("hargaAC"),
                    rs.getInt("hargaCentral"),
                    rs.getInt("hargaKacaFilm")
                ));

            mobilCombobox.getItems().setAll(mobilList);
            mobilCombobox.setOnAction(__ -> {
                Mobil mobil = mobilCombobox.getSelectionModel().getSelectedItem();
                if(mobil != null) {
                    namaTextField.setText(mobil.nama);
                    hargaTextField.setText(String.valueOf(mobil.hargaMobil));
                }
                calcTotal();
            });

            ccdcRB.setToggleGroup(group);
            cashRB.setToggleGroup(group);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    private void onRBChecked(Function<Mobil, Integer> func) {
        Mobil item = mobilCombobox
                .getSelectionModel()
                .getSelectedItem();

        if(item != null)
            hargaCCDCCashTextField.setText(String.valueOf(func.apply(item)));
        calcTotal();
    }

    @FXML
    public void onCCDCRBChecked(ActionEvent actionEvent) {
        onRBChecked(x -> x.hargaCCDC);
    }

    @FXML
    public void onCashChecked(ActionEvent actionEvent) {
        onRBChecked(x -> x.hargaCash);
    }

    private void onCBChecked(CheckBox cb, TextField tf, Function<Mobil, Integer> func) {
        if(cb.isSelected())
            tf.setText(String.valueOf(func.apply(mobilCombobox.getValue())));
        else
            tf.setText("");
        calcTotal();
    }

    @FXML
    public void onACChecked() {
        onCBChecked(acCheckBox, acTextField, x -> x.hargaAC);
    }

    @FXML
    public void onCentralChecked(ActionEvent actionEvent) {
        onCBChecked(centralCheckBox, centralTextField, x -> x.hargaCentral);
    }

    @FXML
    public void onKacaFilmChecked(ActionEvent actionEvent) {
        onCBChecked(kacaCheckBox, kacaTextField, x -> x.hargaKacaFilm);
    }

    public void onClearButton(ActionEvent actionEvent) {
        hargaTextField.clear();
        acTextField.clear();
        kacaTextField.clear();
        centralTextField.clear();
        hargaCCDCCashTextField.clear();
        namaTextField.clear();
        totalTextField.clear();
        mobilCombobox.setValue(null);
    }

    public void onClose(ActionEvent actionEvent) {
        Platform.exit();
    }
}
